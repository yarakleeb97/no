var express = require("express");
var router = express.Router();
const Order = require("../models/Order");

router.post("/order", (req, res, next) => {
  const order = new Order({
    user: req.body.user,
    product: req.body.product,
  });
  order
    .save()
    .then((result) => {
      res.status(200).json({
        message: "add Order",
      });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
      console.log(err);
    });
});

router.patch("/order/:id", (req, res, next) => {
  const newProduct = req.body.product;
  Order.find({ _id: req.params.id })
    .then((result) => {
      var oldProduct = result[0].product;
      for (
        var indexOfNewProduct = 0;
        indexOfNewProduct < newProduct.length;
        indexOfNewProduct++
      ) {
        for (
          var indexOfOldProduct = 0;
          indexOfOldProduct < newProduct.length;
          indexOfOldProduct++
        ) {
          if (
            newProduct[indexOfNewProduct]._id ===
            oldProduct[indexOfOldProduct]._id
          ) {
            oldProduct[indexOfOldProduct].quantity =
              newProduct[indexOfNewProduct].quantity;
            newProduct.splice(indexOfNewProduct, 1);
            break;
          }
        }
      }
      oldProduct = oldProduct.concat(newProduct);
      const newOrder = {
        product: oldProduct,
      };
      Order.updateOne({ _id: req.params.id }, { $set: newOrder })
        .then((doc) => {
          res.status(202).json({
            message: "updated product",
            data: doc,
          });
        })
        .catch((err) => {
          res.status(404).json({
            message: err,
          });
        });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.get("/order", function (req, res, next) {
  Order.find()
    .populate("user", "_id username")
    .then((doc) => {
      res.status(200).json({
        message: "get all orders",
        data: doc,
      });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.get("/order/:id", function (req, res, next) {
  Order.findById({ _id: req.params.id })
    .populate("user", "_id username")
    .then((doc) => {
      res.status(200).json({
        message: "get  order",
        data: doc,
      });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});
router.delete("/order", (req, res, next) => {
  Order.remove({})
    .then((result) => {
      if (result) {
        res.status(200).json({
          message: "delete all orders",
        });
      } else {
        res.status(404).json({
          message: "error",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(404).json({
        message: err,
      });
    });
});
module.exports = router;
