var express = require("express");
var router = express.Router();
const Product = require("../models/Product");
const multer = require("multer");
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./productImage/");
  },
  filename: function (req, file, cb) {
    cb(null, new Date().toDateString() + file.originalname);
  },
});
const upload = multer({ storage: storage, limits: 1024 * 1024 * 5 });
router.post("/product", upload.single("myfile"), (req, res, next) => {
  Product.find({ name: req.body.name })
    .then((result) => {
      if (result.length < 1) {
        const product = new Product({
          name: req.body.name,
          price: req.body.price,
          description: req.body.description,
          image: req.file.path,
        });
        product
          .save()
          .then((result) => {
            console.log(product);
            res.status(200).json({
              message: "created product",
            });
          })
          .catch((err) => {
            res.status(404).json({
              message: err,
            });
            console.log(err);
          });
      } else {
        res.status(404).json({
          message: "already product exist",
        });
      }
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.patch("/product/:id", (req, res, next) => {
  const newProduct = {
    name: req.body.name,
    price: req.body.price,
    description: req.body.description,
  };
  Product.findByIdAndUpdate({ _id: req.params.id }, { $set: newProduct })
    .then((result) => {
      if (result) {
        res.status(202).json({
          message: "updated product",
          data: result,
        });
      } else {
        res.status(404).json({
          message: "product not found",
        });
      }
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.get("/product", function (req, res, next) {
  Product.find()
    .select("_id name price description image")
    .then((doc) => {
      const products = {
        doc: doc.map((doc) => {
          return {
            _id: doc._id,
            name: doc.name,
            price: doc.price,
            description: doc.description,
            image: doc.image,
            url: {
              type: "GET",
              url: "http://127.0.0.1:3000/products/product/" + doc._id,
            },
          };
        }),
      };
      res.status(200).json({
        message: "get all products",
        data: products,
      });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.get("/product/:id", function (req, res, next) {
  Product.findById({ _id: req.params.id })
    .then((result) => {
      if (result) {
        res.status(200).json({
          message: "sus",
          data: result,
        });
      } else {
        res.status(404).json({
          message: "product  not found",
        });
      }
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});

router.delete("/product/:id", (req, res, next) => {
  Product.findByIdAndDelete({ _id: req.params.id })
    .then((result) => {
      if (result) {
        res.status(200).json({
          message: "delete Product",
        });
      } else {
        res.status(404).json({
          message: "Product not found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(404).json({
        message: err,
      });
    });
});
module.exports = router;
