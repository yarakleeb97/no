var express = require("express");
var router = express.Router();
const bcrypt = require("bcrypt");
const User = require("../models/User");
const { request, response } = require("express");

router.post("/signup", (req, res, next) => {
  User.find({ username: req.body.username })
    .then((result) => {
      // console.log(result);
      if (result.length < 1) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            res.status(404).json({ message: err });
          } else {
            const user = new User({
              username: req.body.username,
              password: hash,
            });
            user
              .save()
              .then((result) => {
                console.log(result);
                res.status(200).json({
                  message: "created acount",
                });
              })
              .catch((err) => {
                res.status(404).json({
                  message: err,
                });
                console.log(err);
              });
          }
        });
      } else {
        res.status(404).json({
          message: "already exist",
        });
      }
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});
router.patch("/user/:id", (req, res, next) => {
  bcrypt
    .hash(req.body.password, 10)
    .then((hash) => {
      const newuser = {
        username: req.body.username,
        password: hash,
      };
      User.findByIdAndUpdate({ _id: req.params.id }, { $set: newuser })
        .then((result) => {
          if (result) {
            res.status(202).json({
              message: "updated user",
              data: result,
            });
          } else {
            res.status(404).json({
              message: "user not found",
            });
          }
        })
        .catch((err) => {
          res.status(404).json({
            message: err,
          });
        });
    })
    .catch((err) => {
      res.status(404).json({
        message: err,
      });
    });
});
router.delete("/user/:id", (req, res, next) => {
  console.log(User.findById({ _id: req.params.id }));
  User.findByIdAndDelete({ _id: req.params.id })
    .then((result) => {
      if (result) {
        res.status(200).json({
          message: "delete user",
          data: result,
        });
      } else {
        res.status(404).json({
          message: "user not found",
        });
      }
    })
    .catch((err) => {
      console.log(err);
      res.status(404).json({
        message: err,
      });
    });
});
module.exports = router;
